# FLISOL 2022
18º Festival Latinoamericano de Instalación de Software Libre el sabado 23 de abril 2022. En Panamá se estara celebrando en diferente localidades. https://flisol.info/
#### Fecha:  23 de Abril del 2022
#### Lugar: Auditorio de  [Universidad Interamericana de Panama](https://goo.gl/maps/nDuLBit9vjCmL4np9)
#### Horario: 9:00 a 16:30
### [*Registrate* ](https://floss-pa.net/events/FLISOL-2022) es gratis


|       EXPOSITORES  |                           TEMA                 |    CATEGORÍA   | HORARIO  |
|--------------------|------------------------------------------------|----------------|----------|
|FLOSSPA Team        | Bienvenida   FLISOL                            |                |09:00 - 09:30  |
| [Arnulfo Reyes](https://twitter.com/arnuIfo_07)  | No le temas a la línea de comandos - Fedora Forever | Conocimiento General |09:30 - 10:15  |
| [Juan Moises Torrijos](http://moitorrijos.com) | Enviando Formularios Web con Formdata y fetch  | Diseño y Programacion |10:15 - 11:00     |
| Luis Segundo |  ¿Qué es el software libre y como contribuyo? |  Conocimiento General |11:00 - 11:45  |
| [Kevin Kantule]() | Automatizando con la ayuda del trabajo colaborativo entre chatbots y RPA. | Automatización/RPA  |11:45 - 12:30  |
| **Almuerzo**            | **Almuerzo**                                        |                |12:30 - 13:00 |
|  Jonathan Guaynora | Mi experiencia como estudiante UIP usando Linux - Fedora  | Conocimiento General  |13:00 - 13:30  |
| Rogelio Morrel | Datos, NFT y Metaverso usando du .  | Cripto  |13:30 - 14:15  |
| [Ismael Valderrama](https://linkedin.com/in/ismael-valderrama-67933b17) | ¿Por qué otro generador de sitios web estaticos (SSG) más?   | Programacion  |14:15 - 15:00  |
| [Gonzalo Nina M.](https://twitter.com/lorddemon) |  Fedora Security Project |  Ciberseguridad |15:00 - 15:45  |
| [Roberto Rubio]() |  Despliegue Automatizado de Aplicaciones con CI/CD en Docker y Kubernetes | Nube/DevOps/Automatización  |15:45 - 16:30  |

## Organizado por:
- ### [Fundacion Panameña Tecnologias Libre](https://floss-pa.net/), [Comunidad de Floss Panamá](https://floss-pa.net/) y Falculta de Ingenieria UIP
- ###### [Maryon Torres](https://twitter.com/maryitotr),[Adbel Martinez](https://twitter.com/abdelgmartinezl), [Ismael Valderrama](https://www.linkedin.com/in/ismael-valderrama-67933b17), [Luis Segundo](https://blackfile.dev), [Danilo](), [Alejandro Perez]()


## Patrocinadores
[<img src="https://portal.uip.edu.pa/resources/images/institutions/banner_uip.png" width="170">](https://www.uip.edu.pa) 
[<img src="https://pbs.twimg.com/profile_images/852597051808522240/5iJqsWQL_400x400.jpg" width="170">](https://floss-pa.net) 
[<img src="https://s3.us-east-1.amazonaws.com/cdn.soluprime.io/logo_h.png" width="170">](http://soluprime.io) 
[<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Fedora_logo_and_wordmark.svg/800px-Fedora_logo_and_wordmark.svg.png?20170315143305" width="170">](https://getfedora.org) 
[<img src="https://cytlastechnology.com/wp-content/uploads/2021/02/Cytlas-2-Sin-fondo.png" alt="Cytlas Technology Labs" width="170">](https://cytlastechnology.com)


